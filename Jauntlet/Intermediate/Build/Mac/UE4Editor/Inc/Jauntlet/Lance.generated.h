// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
struct FVector;
struct FHitResult;
#ifdef JAUNTLET_Lance_generated_h
#error "Lance.generated.h already included, missing '#pragma once' in Lance.h"
#endif
#define JAUNTLET_Lance_generated_h

#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_SelfActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		this->OnHit(Z_Param_SelfActor,Z_Param_OtherActor,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
	}


#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_SelfActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		this->OnHit(Z_Param_SelfActor,Z_Param_OtherActor,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
	}


#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesALance(); \
	friend JAUNTLET_API class UClass* Z_Construct_UClass_ALance(); \
	public: \
	DECLARE_CLASS(ALance, APawn, COMPILED_IN_FLAGS(0), 0, Jauntlet, NO_API) \
	DECLARE_SERIALIZER(ALance) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ALance*>(this); }


#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_INCLASS \
	private: \
	static void StaticRegisterNativesALance(); \
	friend JAUNTLET_API class UClass* Z_Construct_UClass_ALance(); \
	public: \
	DECLARE_CLASS(ALance, APawn, COMPILED_IN_FLAGS(0), 0, Jauntlet, NO_API) \
	DECLARE_SERIALIZER(ALance) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ALance*>(this); }


#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALance); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ALance(const ALance& InCopy); \
public:


#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ALance(const ALance& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALance)


#define Jauntlet_Source_Jauntlet_Public_Lance_h_8_PROLOG
#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Jauntlet_Source_Jauntlet_Public_Lance_h_11_RPC_WRAPPERS \
	Jauntlet_Source_Jauntlet_Public_Lance_h_11_INCLASS \
	Jauntlet_Source_Jauntlet_Public_Lance_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Jauntlet_Source_Jauntlet_Public_Lance_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Jauntlet_Source_Jauntlet_Public_Lance_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Jauntlet_Source_Jauntlet_Public_Lance_h_11_INCLASS_NO_PURE_DECLS \
	Jauntlet_Source_Jauntlet_Public_Lance_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Jauntlet_Source_Jauntlet_Public_Lance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
