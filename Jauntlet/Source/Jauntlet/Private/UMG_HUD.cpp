// Fill out your copyright notice in the Description page of Project Settings.

#include "Jauntlet.h"
#include "JauntletGameMode.h"
#include "UMG_HUD.h"


int32 UUMG_HUD::GetRoundNum()
{
	AGameMode* GameMode = GetWorld()->GetAuthGameMode();
	AJauntletGameMode* JGameMode = Cast<AJauntletGameMode>( GameMode ); //downcast

	if (JGameMode)
	{
		return JGameMode->GM_GetRoundNum();
	}
	return 0;
}

int32 UUMG_HUD::GetP1Wins()
{
	AGameMode* GameMode = GetWorld()->GetAuthGameMode();
	AJauntletGameMode* JGameMode = Cast<AJauntletGameMode>( GameMode ); //downcast

	if (JGameMode)
	{
		return JGameMode->GM_GetP1Wins();
	}
	return 0;
}

int32 UUMG_HUD::GetP2Wins()
{
	AGameMode* GameMode = GetWorld()->GetAuthGameMode();
	AJauntletGameMode* JGameMode = Cast<AJauntletGameMode>( GameMode ); //downcast

	if (JGameMode)
	{
		return JGameMode->GM_GetP2Wins();
	}
	return 0;
}