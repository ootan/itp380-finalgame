// Fill out your copyright notice in the Description page of Project Settings.
#include "Jauntlet.h"
#include "SpawnManager.h"
#include "JauntletCharacter.h"
#include "Engine/TargetPoint.h"

// Sets default values
ASpawnManager::ASpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnManager::BeginPlay()
{
	Super::BeginPlay();
    OnSpawnTimer();
}

// Called every frame
void ASpawnManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

    //if player1 dead, and player2 alive, then kill player2 and respawn both of them
    //if player2 dead, and player1 alive, then kill player1 and respawn both of them
    //if both players are dead, then simply respawn both of them
}

void ASpawnManager::OnSpawnTimer()
{
    int numElements = SpawnPoints.Num();
    if(numElements == 0)
    {
        return;
    }
    for(int i = 0; i < numElements; i++)
    {
        auto Char = GetWorld()->SpawnActor<AJauntletCharacter>(CharacterClass, SpawnPoints[i]->GetActorLocation(), SpawnPoints[i]->GetActorRotation());
        if(Char)
        {
            Char->SpawnDefaultController();
        }
    }
}