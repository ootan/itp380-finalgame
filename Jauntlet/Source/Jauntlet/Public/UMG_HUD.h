// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "Blueprint/UserWidget.h"

#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"

#include "UMG_HUD.generated.h"


/**
 * 
 */
UCLASS()
class JAUNTLET_API UUMG_HUD : public UUserWidget
{
	GENERATED_BODY()
	
	
public: 
	UFUNCTION( BlueprintCallable, Category = "HUD" )
	int32 GetRoundNum();	

	UFUNCTION( BlueprintCallable, Category = "HUD" )
	int32 GetP1Wins();

	UFUNCTION( BlueprintCallable, Category = "HUD" )
	int32 GetP2Wins();

	UFUNCTION( BlueprintImplementableEvent, Category = "HUD" )
	void ShowWinner( int32 winnerIndex ); // doesn't have a c++ impl due to BlueprintImplementableEvent
};
